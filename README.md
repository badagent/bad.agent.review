# Mission

This repo captures bad real estate agent reviews from trust worthy sources.

The repo will only expose bad review to avoid fake good reviews (which you see in almost every other real estate website).

## Why Bitbucket/Git Repo？

My [initial github repo](https://github.com/badagentreview/bad.agent.review) was blocked for unknown reason, I am in contact with the support team.

Git repo provides an open source, version controlled history of all changes made to this repository. 

This avoids: 
1. Bad review being removed without a trace.
2. Fake review account.


# Bad Agent List

### [Renna Shee](https://rennashee.com/), 01906557 (CA)
***TL;DR***
Renna tricked the seller to sell $400k under market value, purchased the property and re-listed under her own management.
- Exposed by [1point3acres](https://www.1point3acres.com/bbs/thread-764477-1-1.html).
- [Renna's reply](https://www.1point3acres.com/bbs/thread-764775-1-1.html)


### [Himmat Grewal](https://www.everhomere.com/members/himmat-grewal/) 01935193 (CA)

***TL;DR***
Forced the buyer to use his loan agent, and dishonst on seller's information.
- Exposed from [Blind](https://www.teamblind.com/post/TDObH06f).
